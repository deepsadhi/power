Name    {{ $data.profile.name }} <br />
Location    {{ $data.profile.location }} <br />
Type    {{ $data.profile.type }} <br />
fund    {{ $data.profile.fund }} <br />
fund_amount    ${{ $data.profile.fund_amount }}M <br />
capacity    {{ $data.profile.capacity }} MW <br />
annual energy    {{ $data.profile.annual_energy }} GWh <br />
start    {{ $data.profile.start }} <br />
end    {{ $data.profile.end }} <br />
status    {{ $data.profile.status }}% <br />
remain {{ $data.remain->days }}days <br />
demand {{ $data.calc.de }} <br />
supply {{ $data.calc.su }} <br />
load {{ $data.calc.ls_hr }} <br />
new supply {{ $data.calc.su_new }} <br/>
load new {{ $data.calc.ls_hr_new }} <br/>


