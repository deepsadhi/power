{{include file="header.tpl"}}
<div class="wrapper">
<h1>Onging Hydropower Projects</h1>
<ul id="projects">
{{foreach from=$data.const_list key=k item=v}}
  <li><a href="view.php?id={{$v.id}}">{{$v.name}}</a></li>
{{foreachelse}}
  Array is empty
{{/foreach}}
</ul>

<div><div class="demand"><div class="title">Power Demand</div><div class="data">{{ $data.calc.de }} </div></div></div>
<div class="left">
	<h2>Current</h2>
	<div><div class="title">Shed</div><div class="data">{{ $data.calc.su }}</div></div>
	<div><div class="title">Current Loadshedding</div><div class="data"> {{ $data.calc.ls_hr }}</div></div>
</div>

<div class="right">
	<h2>After the completion of Projects</h2>
	<div><div class="title">New Shed</div><div class="data"> {{ $data.calc.su_new }} </div></div>
	<div><div class="title">New Loadshedding</div><div class="data"> {{ $data.calc.ls_hr_new }}</div></div>
</div>