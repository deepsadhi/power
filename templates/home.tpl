
  <div>
    <form action="{{ $data.submitURL|default:'Undefined' }}" method="post">
      <fieldset>
        <legend>{{ $data.msg|default:'Sign In' }}</legend>
        <label for="email">Email</label>
        <input type="text" name="email" id="email" value="{{ $data.email|default:'' }}" />
        <label for="password">Password</label>
        <input type="password" name="password" id="password" value="{{ $data.password|default:'' }}" />
        <input type="hidden" name="token" value="{{ $data.token|default:'Undefined' }}" />
        <input type="submit" name="signin" value="Sign In" />
        or <a href="signup.php">Sign Up</a>
      </fieldset>
    </form>
  </div>

