<h1>{{ $data.user.name }}</h1>
<a href="logout.php">Logout</a>
<hr />
  <div>
    <form action="{{ $data.submitURL|default:'Undefined' }}" method="post">
      <fieldset>
        <legend>{{ $data.msg|default:'Update user profile' }}</legend>
        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="{{ $data.name|default:'' }}" />
        <label for="email">Email</label>
        <input type="text" name="email" id="email" value="{{ $data.email|default:'' }}" />
        <label for="password">Password</label>
        <input type="password" name="password" id="password" value="{{ $data.password|default:'' }}" />
        <input type="hidden" name="token" value="{{ $data.token|default:'Undefined' }}" />
        <input type="submit" name="signup" value="Update" />
        or <a href="./view.php?id={{ $data.id }}">Cancel</a>
      </fieldset>
    </form>
  </div>
