
  <div>
    <form action="{{ $data.submitURL|default:'Undefined' }}" method="post">
      <fieldset>
        <legend>{{ $data.msg|default:'Please fill up the form' }}</legend>
        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="{{ $data.name|default:'' }}" />
        <label for="email">Email</label>
        <input type="text" name="email" id="email" value="{{ $data.email|default:'' }}" />
        <label for="password">Password</label>
        <input type="password" name="password" id="password" value="{{ $data.password|default:'' }}" />
        <input type="hidden" name="token" value="{{ $data.token|default:'Undefined' }}" />
        <input type="submit" name="signup" value="Sign Up" />
        or <a href="./home.php">Cancel</a>
      </fieldset>
    </form>
  </div>

