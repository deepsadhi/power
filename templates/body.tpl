{{include file="header.tpl"}}
<div class="wrapper">
	<h1 id="project-name">{{ $data.profile.name }}</h1>
	<div id="info">
		<div class="row">
			<div class="header"><img class="header-img" src="img/location.png"/></div>
			<div class="data">{{ $data.profile.location }}</div>
		</div>
		<div class="row">
			<div class="header"><img class="header-img" src="img/energy.png"/></div>
			<div class="data">{{ $data.profile.capacity }}MW</div>
		</div>
		<div class="row">
			<div class="header"><img class="header-img" src="img/fund.png"/></div>
			<div class="data">${{ $data.profile.fund_amount }}M</div>
		</div>
		<div class="row">
			<div class="header"><img class="header-img" src="img/completion_time.png"/></div>
			<div class="data">{{ $data.remain->days }} days</div>
		</div>
		<div class="start-time">{{ $data.profile.start }}</div>-<div class="end-time">{{ $data.profile.end }}</div>
	</div>
	<div><div class="demand"><div class="title">Power Demand</div><div class="data">{{ $data.calc.de }} </div></div></div>
<div class="left">
	<h2>Current</h2>
	<div><div class="title">Shed</div><div class="data">{{ $data.calc.su }}</div></div>
	<div><div class="title">Current Loadshedding</div><div class="data"> {{ $data.calc.ls_hr }}</div></div>
</div>

<div class="right">
	<h2>After the completion of Projects</h2>
	<div><div class="title">New Shed</div><div class="data"> {{ $data.calc.su_new }} </div></div>
	<div><div class="title">New Loadshedding</div><div class="data"> {{ $data.calc.ls_hr_new }}</div></div>
</div>
</div>
