{{* Including a template file *}}
{{include file="header.tpl"}}


<div>
  {{* Printing variable *}}
  {{ $var|default:'Undefined' }}
</div>


<div>
  {{* Printing element of array *}}
  {{ $array.key|default:'Undefined' }}
</div>


<div>
  {{* Create dropdown menu, with php array *}}
  {{html_options id=foo name=foo options=$myOptions selected=$mySelect}}
</div>


<ul>
  {{* Looping an array *}}
  {{foreach from=$myOptions key=k item=v}}
     <li><b>{{$k}}</b>: {{$v}}</li>
  {{foreachelse}}
    Array is empty
  {{/foreach}}
</ul>
