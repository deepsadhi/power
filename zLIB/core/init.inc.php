<?php

/**
 * DIRECTORY_SEPERATOR \ for Windows and / for UNIX a like systems
 */
defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

/**
 * LIB directory
 */
defined('LIB_PATH') ? null : define('LIB_PATH', dirname(__DIR__));

/**
 * Check and load essential files
 */
{
    $locations = array( LIB_PATH.DS.'config'.DS.'config.inc.php',
                        LIB_PATH.DS.'inc'.DS.'constants.inc.php',
                        LIB_PATH.DS.'inc'.DS.'loader.inc.php',
                        LIB_PATH.DS.'inc'.DS.'error.inc.php',
                      );
    foreach($locations as $loc){
        if(!file_exists($loc)){
            die('<em>'.$loc.'</em> does not exist :(');
        } else if(!is_readable($loc)) {
            die('<em>'.$loc.'</em> is not readable :( Give read permission');
        } else {
            require_once($loc);
        }
    }
}

/**
 * Development environment 
 * Constants are defined in zLIB/inc/constants.inc.php
 */
if(Config\DEV_ENV){
    error_reporting(E_ALL | E_STRICT);
}else{
    {
        error_reporting(0);
        ini_set("log_errors", 1);
        $loc = dirname(LIB_PATH).DS.'logs'.DS.'errors.log';
        if(!is_writeable(dirname($loc))){
            die('<em>'.dirname($loc).'</em> is not writeable :( Give write permission');
        }
        if(file_exists($loc) && !is_writeable($loc)){
            die('<em>'.$loc.'</em> is not writeable :( Give write permission');
        }
        ini_set("error_log", $loc);
    }
}

/**
 * Load log4php
 */
if(Config\LOAD_LOG4PHP){
    Config\loadfile(LIB_PATH.DS.'vendor'.DS.'log4php'.DS.'Logger.php');
    {
        $loc = LIB_PATH.DS.'inc'.DS.'log4php.inc.php';
        if(!file_exists($loc)){
            die('<em>'.$loc.'</em> does not exist :(');
        } else if(!is_readable($loc)) {
            die('<em>'.$loc.'</em> is not readable :( Give read permission');
        }
    }
    Logger::configure(LIB_PATH.DS.'inc'.DS.'log4php.inc.php');
    {
        $loc = dirname(LIB_PATH).DS.'logs'.DS.'file.log';
        if(!is_writeable(dirname($loc))){
            die('<em>'.dirname($loc).'</em> is not writeable :( Give write permission');
        }
        if(file_exists($loc) && !is_writeable($loc)){
            die('<em>'.$loc.'</em> is not writeable :( Give write permission');
        }
    }
}

/**
 * Load Smarty templating library
 */
if(Config\LOAD_SMARTY){
    Config\loadFile(LIB_PATH.DS.'vendor'.DS.'smarty'.DS.'Smarty.class.php');
    $dirs = array(  dirname(LIB_PATH).DS.'cache'.DS,
                    dirname(LIB_PATH).DS.'templates_c'.DS,
                 );
    Config\checkWritePermission($dirs);
}

/**
 * Load filterus
 * Generate an anti-CSRF token if one doesn't exist
 */
if(Config\PREPARE_FORM){
    if(session_id() == ''){
        session_start();
    }
    if(!isset($_SESSION['token'])){
        $_SESSION['token'] = sha1(uniqid(mt_rand(), true));
    }
    Config\loadFile(LIB_PATH.DS.'vendor'.DS.'htmlpurifier'.DS.'HTMLPurifier.standalone.php');
}

/**
 * Session check
 */
if(defined('Config\SESSION_CHECK')){
    if(session_id() == ''){
        session_start();
    }
    if(!isset($_SESSION[Config\SESSION_NAME])){
        header("Location: ./");
    }
}

/**
 * Set error handler
 */
#@set_error_handler('Error\errorHandler');

/**
 * Set exception handler
 */
#@set_exception_handler('Error\exceptionHandler');

/**
 * Auto load function for classes
 *
 * @param string $class Name of class to be instanciated
 */
spl_autoload_register (function ($class) {
    $filename = LIB_PATH.DS.'class'.DS.strtolower($class).'.class.php';
    Config\loadFile($filename);
    if($class=='Smarty_Template'){

    }
});
