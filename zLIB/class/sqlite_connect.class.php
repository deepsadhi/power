<?php

/* SQLite SQL */
    
/**
 * SQLite SQL (query, insert, update, delete) 
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to the ... License, available
 * at http://
 *
 * @author      Deepak Adhikari <deeps.adhi@gmail.com>
 * @copyright   2013
 * @license     http://
 * @version     1.0.0
 */

/**
 * Load SQLite configuration file
 */
Config\loadFile(LIB_PATH.DS.'inc'.DS.'constants.inc.php');

class SQLite_Connect
{
    /**
     * Stores a SQLite object
     *
     * @var object A SQLite object
     */
    private static $_db;


    /**
     * Protect from cloning 
     */
    private function __clone() {}

    /**
     * Protect from wakeup
     */
    private function __wakeup() {}

    /**
     * Connects to SQLite
     */
    private function __construct(){
        self::checkFilePermissions();
        /**
         * Define constants for configuration info
         * Constants are defined in zLIB/config/config.inc.php
         */
        try{
            @self::$_db = new PDO('sqlite:'.Config\SQLITE_DB_LOC);
            @self::$_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        } catch(Exception $e){
            /**
             * If the DB connection fails, log error
             */
            Error\report('fatal', DB_ERROR, $e->getMessage(), __CLASS__);
        }
    }

    /**
     * Checks for a DB object and creates one if it's not created
     *
     * @return object $_db A SQLite object
     */
     public static function getConnection(){
        if(!self::$_db){
            new self();
        }
        return self::$_db;
    }
    
    private function checkFilePermissions(){
        Config\checkWritePermission(dirname(Config\SQLITE_DB_LOC));
        if(file_exists(Config\SQLITE_DB_LOC)){
            Config\checkReadPermission(Config\SQLITE_DB_LOC);
            Config\checkWritePermission(Config\SQLITE_DB_LOC);
        }
    }
} 
