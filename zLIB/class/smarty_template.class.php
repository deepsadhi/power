<?php

/* Smarty web template system */
    
/**
 * Smarty web template system  
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to the ... License, available
 * at http://
 *
 * @author      Deepak Adhikari <deeps.adhi@gmail.com>
 * @copyright   2013
 * @license     http://
 * @version     1.0.0
 */

class Smarty_Template
{
    /**
     * Stores a smarty object
     *
     * @var object A smarty object
     */
    static private $_instance;
    
    /**
     * Protect from auto construct 
     */
    private function __construct() {}
    
    /**
     * Protect from cloning 
     */
    private function __clone() {}
    
    /**
     * Protect from wakeup 
     */
    private function __wakeup() {}
    
    /**
     * Checks for a smarty object and creates one if it's not created
     *
     * @return object $_instance A smarty object
     */
    static public function instance() {
        if(!isset(self::$_instance)) {
            self::checkPermissions();

            $smarty = new Smarty;

            $smarty->auto_literal = false;
            $smarty->left_delimiter = '{{';
            $smarty->right_delimiter = '}}';

            $smarty->setTemplateDir(dirname(LIB_PATH).DS.'templates'.DS);
            $smarty->setCompileDir(dirname(LIB_PATH).DS.'templates_c'.DS);
            $smarty->setConfigDir(LIB_PATH.DS.'config'.DS.'smarty'.DS);
            $smarty->setCacheDir(dirname(LIB_PATH).DS.'cache'.DS);

            //$smarty->caching = Smarty::CACHING_LIFETIME_CURRENT;
            $smarty->debugging = false;
            
            #define( 'CFG_DIR_TEMPLATES', $smarty->getTemplateDir(0) );
            
            self::$_instance = $smarty;
        }
        return self::$_instance;
    }

    private static function checkPermissions(){
        $dirs = array(  LIB_PATH.DS.'vendor'.DS.'smarty'.DS.'Smarty.class.php',
                        LIB_PATH.DS.'config'.DS.'smarty'.DS,
                        dirname(LIB_PATH).DS.'templates'.DS,
                        dirname(LIB_PATH).DS.'templates_c'.DS,
                        dirname(LIB_PATH).DS.'cache'.DS,
                     );
        Config\checkFileExists($dirs);
        $dirs = array(  dirname(LIB_PATH).DS.'cache'.DS,
                        dirname(LIB_PATH).DS.'templates_c'.DS,
                     );
        Config\checkWritePermission($dirs);
    }
}
