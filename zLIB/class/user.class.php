<?php

/* User table SQL */
    
/**
 * User table SQL (query, insert, update, delete) 
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to the ... License, available
 * at http://
 *
 * @author      Deepak Adhikari <deeps.adhi@gmail.com>
 * @copyright   2013
 * @license     http://
 * @version     1.0.0
 */

/**
 * Load database configuration file
 */
Config\loadFile(LIB_PATH.DS.'class'.DS.'database.class.php');

class User extends Database 
{
    /**
     * User table name
     * @var string Name of user table
     */
    private $_table = Config\USER_TABLE_NAME;

    /**
     * User table unique field
     * @var string Name of the uniqu field
     */
    private $_uniqueField = Config\USER_UNIQUE_FIELD;

    /**
     * Salt length
     * @var int Length of saltCost
     */
     private $_saltLength = Config\SALT_LENGTH;

    /**
     * Connects to database
     */
    public function __construct(){
        /**
         * Call parent constructor
         */
        parent::__construct();
   }

    /**
     * Find user
     *
     * @param   string  $where  Find condition
     * @return  bool            Query executed sucessfully or not
     */
    public function find($where=''){
        if(!empty($where)){
            $sql = 'SELECT * FROM '.$this->_table.' WHERE '.$where;
        }else{
            $sql = 'SELECT * FROM '.$this->_table;
        }
        $res = $this->sql($sql);
        return $res;
    }

    /**
     * Find profile by unique id
     *
     * @param   int  $id  Find condition
     * @return  bool            Query executed sucessfully or not
     */
    public function findProfileById($id){
        $res = $this->findById($this->_table, $id);
        return $res;
    }

    /**
     * Insert into table
     *
     * @param   array   $params User details
     * @return  bool            Row updated or not
     */
    public function add(array $params){
        $params = $this->addDataTypeToParamsKeys($params);
        $res = $this->insert($this->_table, $params);
        return $res; 
    }

    /**
     * Add data type to parameters keys
     *
     * @param    array $params Fields to be inserted with value
     * @return   array Fields to be inserted with value and key value prefixed with str datatype 
     */
    private function addDataTypeToParamsKeys($params){
        foreach($params as $key => $value){
            if($key === 'password'){
                $params['str '.$key] = $this->hash($value);
            }else{
                $params['str '.$key] = $value;
            }
            unset($params[$key]);
        }
        return $params;
    }

    /**
     * Update profile of a user
     *
     * @param   array   $params User details
     * @param   string  $where  Update condition
     * @return  bool            Row updated or not
     */
    public function updateProfile($params, $where=''){
        $res = $this->find($where);
        if($res && $this->data['numResults'] >= 1){
            $params = $this->addDataTypeToParamsKeys($params);
            $res = $this->update($this->_table, $params, $where);
            return $res;
        }
    }

    /**
     * Delete a profile
     *
     * @param   int   $int    Id of row
     * @return  bool          Row deleted or not
     */
    public function deleteProfile($id){
        $res = $this->delete($this->_table, $id);
        return $res; 
    }

    /**
     * Authenticate user
     *
     * @param   array   $params   Username and password to be validated
     * @return  bool              Username and password combination right or wrong
     */
    public function authenticate($params){
        $this->initData();
        $this->sql = 'SELECT * FROM `'.$this->_table.'` WHERE `'.$this->_uniqueField.'`=:username LIMIT 1';
        $this->data['lastQuery'] = $this->sql;
        try {
            $this->stmt = $this->db->prepare($this->sql);
            $this->db->beginTransaction();
            $this->stmt->bindParam(':username', $params[$this->_uniqueField], PDO::PARAM_STR);
            $this->stmt->execute();
            $this->data['results'] = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
            $this->data['numResults'] = count($this->data['results']);
            if($this->data['numResults'] > 0){
                $this->data['keys'] = array_keys($this->data['results'][0]);
            }
            $this->db->commit();
            $this->stmt->closeCursor();
            if($this->data['numResults'] === 1){
                $r = $this->data['results'];
                $res = array_shift($r);
                $password = $res['password'];
                $au = $this->verifyPassword($password, $params['password']);
                if($au){
                    $_SESSION[Config\SESSION_NAME] = array(  'id'    => $res[Config\UNIQUE_ID],
                                                             'name'  => $res['name'],
                                                             'email' => $res[Config\USER_UNIQUE_FIELD]
                                                          );
                    return true;
                }else{
                    return false;
                }
            }
            return false;
        } catch(Exception $e){
            $this->db->rollBack();
            $this->data['lastError'] = $e->getMessage();
            Error\report('fatal', DB_ERROR, $e->getMessage(), __CLASS__);
            return false;
        }
    }

    private function verifyPassword($dbPassword, $password){
        if(crypt($password, $dbPassword) === $dbPassword){
            return true;
        }else{
            return false;
        }
    }

    private function hash($password){
        $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
        $salt = sprintf("$2a$%02d$", $this->_saltLength) . $salt;
        $hash = crypt($password, $salt);
        return $hash;
    }

}
