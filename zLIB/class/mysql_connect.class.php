<?php

/* MySQL SQL */
    
/**
 * MySQL SQL (query, insert, update, delete) 
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to the ... License, available
 * at http://
 *
 * @author      Deepak Adhikari <deeps.adhi@gmail.com>
 * @copyright   2013
 * @license     http://
 * @version     1.0.0
 */

/**
 * Load MySQL configuration file
 */
Config\loadfile(LIB_PATH.DS.'inc'.DS.'constants.inc.php');

class MySQL_Connect
{
    /**
     * Stores a MySQL object
     *
     * @var object A MySQL object
     */
    private static $_db;


    /**
     * Protect from cloning 
     */
    private function __clone() {}

    /**
     * Protect from wakeup
     */
    private function __wakeup() {}

    /**
     * Connects to MySQL
     */
    private function __construct(){
        /**
         * Define constants for configuration info
         * Constants are defined in zLIB/config/config.inc.php
         */
        $dsn = 'mysql:host=' . Config\DB_HOST . ';dbname=' . Config\DB_NAME;
        try{
            @self::$_db = new PDO($dsn, Config\DB_USER, Config\DB_PASS);
            @self::$_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        } catch(Exception $e){
            /**
             * If the DB connection fails, log error
             */
            Error\report('fatal', DB_ERROR, $e->getMessage(), __CLASS__);
        }
    }

    /**
     * Checks for a DB object and creates one if it's not created
     *
     * @return object $_db A MySQL object
     */
     public static function getConnection(){
        if(!self::$_db){
            new self();
        }
        return self::$_db;
    }
} 
