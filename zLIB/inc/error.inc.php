<?php

/* Error reporting */

namespace Error;

/**
 * Load zLIB constants
 */
require_once(LIB_PATH.DS.'inc'.DS.'constants.inc.php');

/**
 * Report errors
 *
 * @param string    $level      Error level
 * @param int       $errorCode  Error Code
 * @param string    $errorMsg   Error message
 * @param string    $source     Point where error encountered
 */
function report($level='fatal', $errorCode, $errorMsg, $source){
    if(\Config\LOAD_LOG4PHP){
        $log = \Logger::getLogger($source);
        $log->$level($errorCode.': '.$errorMsg);
    }
    /**
     * Prompt error message
     */
    if(\Config\DISP_ERROR){
        die($errorCode.': '.$errorMsg);
    }else{
        die('Something is wrong :(');
    }
}

/**
 * Error handler
 *
 * @param int       $errno      Error number
 * @param string    $errstr     Error in sentence
 * @param string    $errfile    File in which error occured
 * @param int       $errline    Line number in which error encountered
 */
function errorHandler ($errno, $errstr, $errfile, $errline){
    namespace\report('fatal', ERROR_HANDLER, $errno.' '.$errstr.' '.$errfile.' '.$errline, 'Error Handler');
}

/**
 * Exception handler
 *
 * @param object Exception object
 */
function exceptionHandler(\Exception $e) {
    namespace\report('fatal', \Config\EXCEPTION_HANDLER, $e->getMessage(), 'Exception Handler');
}
