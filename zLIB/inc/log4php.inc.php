<?php

require_once(LIB_PATH.DS.'inc'.DS.'constants.inc.php');

return array(
    'appenders' => array(
        'default' => array(
            'class' => 'LoggerAppenderRollingFile',
            'layout' => array(
                'class' => 'LoggerLayoutPattern',
                'params' => array(
                    'conversionPattern' => '%date{d.m.Y H:i:s,u} | %logger %-5level %msg%n'
                )
            ),
            'params' => array(
                'file' => dirname(LIB_PATH).DS.'logs'.DS.'file.log',
                'maxFileSize' => Config\MAX_FILE_SIZE,
                'maxBackupIndex' => Config\MAX_BACKUP_INDEX,
            ),
        ),
    ),
    'rootLogger' => array(
        'appenders' => array('default'),
        'level' => 'WARN',
    ),
);
