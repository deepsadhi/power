<?php

/* Define constants of library configuration */

namespace Config;

/**
 * Load configuration variables
 */
require_once(LIB_PATH.DS.'config'.DS.'config.inc.php');

/**
 * Define DEV_ENV if not defined
 */
defined(__NAMESPACE__.'\\'.'DEV_ENV')           ? null : define(__NAMESPACE__.'\\'.'DEV_ENV',                   $devEnv);

/**
 * Define DISP_ERROR if not defined
 */
defined(__NAMESPACE__.'\\'.'DISP_ERROR')        ? null : define(__NAMESPACE__.'\\'.'DISP_ERROR',                $dispError);

/**
 * Error codes
 */
defined(__NAMESPACE__.'\\'.'FILE_NOT_EXIST')    ? null : define(__NAMESPACE__.'\\'.'FILE_NOT_EXIST',            804);
defined(__NAMESPACE__.'\\'.'FILE_NOT_READABLE') ? null : define(__NAMESPACE__.'\\'.'FILE_NOT_READABLE',         800);
defined(__NAMESPACE__.'\\'.'FILE_NOT_WRITEABLE')? null : define(__NAMESPACE__.'\\'.'FILE_NOT_WRITEABLE',        801);
defined(__NAMESPACE__.'\\'.'ERROR_HANDLER')     ? null : define(__NAMESPACE__.'\\'.'ERROR_HANDLER',             803);
defined(__NAMESPACE__.'\\'.'EXCEPTION_HANDLER') ? null : define(__NAMESPACE__.'\\'.'EXCEPTION_HANDLER',         805);
defined(__NAMESPACE__.'\\'.'DB_ERROR')          ? null : define(__NAMESPACE__.'\\'.'DB_ERROR',                  802);

/**
 * For SQLite database
 */
if(isset($sqlLiteDBLoc)){
/**
 * Define SQLite database location
 */
defined(__NAMESPACE__.'\\'.'SQLITE_DB_LOC')     ? null : define(__NAMESPACE__.'\\'.'SQLITE_DB_LOC',             $sqlLiteDBLoc);
}

/**
 * For MySQL database
 */
if(isset($mysqlConf)){
/**
 * Define constants for mysql configuration
 */
foreach($mysqlConf as $name => $val){
defined(__NAMESPACE__.'\\'.$name)               ? null : define(__NAMESPACE__.'\\'.$name,                       $val);
}
}

/**
 * For table unique id field name
 */
defined(__NAMESPACE__.'\\'.'UNIQUE_ID')         ? null : define(__NAMESPACE__.'\\'.'UNIQUE_ID',                 $uniqueId);

/**
 * For users table
 */
defined(__NAMESPACE__.'\\'.'USER_TABLE_NAME')   ? null : define(__NAMESPACE__.'\\'.'USER_TABLE_NAME',           $tableName);
defined(__NAMESPACE__.'\\'.'USER_UNIQUE_FIELD') ? null : define(__NAMESPACE__.'\\'.'USER_UNIQUE_FIELD',         $uniqueField);

/**
 * Salt length
 */
defined(__NAMESPACE__.'\\'.'SALT_LENGTH')       ? null : define(__NAMESPACE__.'\\'.'SALT_LENGTH',               $saltLength);

/**
 * log4php config constants
 */
defined(__NAMESPACE__.'\\'.'LOAD_LOG4PHP')      ? null : define(__NAMESPACE__.'\\'.'LOAD_LOG4PHP',              $loadLog4Php);
defined(__NAMESPACE__.'\\'.'MAX_FILE_SIZE')     ? null : define(__NAMESPACE__.'\\'.'MAX_FILE_SIZE',             $maxFileSize);
defined(__NAMESPACE__.'\\'.'MAX_BACKUP_INDEX')  ? null : define(__NAMESPACE__.'\\'.'MAX_BACKUP_INDEX',          $maxBackupIndex);

/**
 * Load Smarty templating library
 */
defined(__NAMESPACE__.'\\'.'LOAD_SMARTY')      ? null : define(__NAMESPACE__.'\\'.'LOAD_SMARTY',                $loadSmarty);

/**
 * Prepare form
 */
defined(__NAMESPACE__.'\\'.'PREPARE_FORM')      ? null : define(__NAMESPACE__.'\\'.'PREPARE_FORM',              $prepareForm);

/**
 * Session name
 */
defined(__NAMESPACE__.'\\'.'SESSION_NAME')      ? null : define(__NAMESPACE__.'\\'.'SESSION_NAME',              'user');
