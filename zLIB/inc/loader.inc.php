<?php

/* File loader and permission checks */

namespace Config;

/**
 * Load zLIB constants
 */
require_once(LIB_PATH.DS.'inc'.DS.'constants.inc.php');

/**
 * If not array convert to array
 *
 * @param   string|array    $param  Paramaters either string or array
 */
function toArray($param){
    if(!is_array($param)){
        $param = array($param);
    }
    return $param;
}

/**
 * Load file(s)
 *
 * @param   string|array   $locations  Location(s) of file(s)
 */
function loadFile($locations){
    $locations = namespace\toArray($locations);
    namespace\checkFileExists($locations);
    namespace\checkReadPermission($locations);
    foreach($locations as $loc){
        require_once($loc);
    }
}

/**
 * Check file exists or not
 *
 * @param   string|array   $locations  Location(s) of file(s)
 */
function checkFileExists($locations){
    $locations = namespace\toArray($locations);
    foreach($locations as $loc){
        if(!file_exists($loc)){
            \Error\report('fatal', FILE_NOT_EXIST, $loc.' does not exist', 'Load file');
        }
    }
}

/**
 * Check read permission
 *
 * @param   string|array   $locations  Location(s) of file(s)
 */
function checkReadPermission($locations){
    $locations = namespace\toArray($locations);
    foreach($locations as $loc){
        if(!is_readable($loc)){
            \Error\report('fatal', FILE_NOT_READABLE, $loc.' is not readable', 'Load file');
        }
    }
}

/**
 * Check write permission
 *
 * @param   string|array   $locations  Location(s) of file(s)
 */
function checkWritePermission($locations){
    $locations = namespace\toArray($locations);
    foreach($locations as $loc){
        if(!is_writeable($loc)){
            \Error\report('fatal', FILE_NOT_WRITEABLE, $loc.' is not writeable', 'Load file');
        }
    }
}
