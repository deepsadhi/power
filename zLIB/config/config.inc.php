<?php

/* Conigure zLIB */

namespace Config;

# SQLite database
# $sqlLiteDBLoc         = dirname(LIB_PATH).DS.'sql'.DS.'db.sqlite3';

# MySQL database
  $mysqlCon             = array();
  $mysqlConf['DB_HOST'] = 'localhost';
  $mysqlConf['DB_USER'] = 'root';
  $mysqlConf['DB_PASS'] = '';
  $mysqlConf['DB_NAME'] = 'power';

# Table unique id field name
  $uniqueId             = 'id';

# Users table
  $tableName            = 'users';
  $uniqueField          = 'email';

# Salt Length
  $saltLength           = 10;

# Development Environment
  $devEnv               = true;

# Display critical error
  $dispError            = true;

# Log4php library 
  $loadLog4Php          = true;
  $maxFileSize          = '1MB';
  $maxBackupIndex       = 5;

# Smarty templating library
  $loadSmarty           = true;

# Prepare form
  $prepareForm          = false;
