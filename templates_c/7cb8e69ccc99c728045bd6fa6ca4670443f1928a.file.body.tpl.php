<?php /* Smarty version Smarty-3.1.16, created on 2014-02-22 11:48:01
         compiled from "C:\xampp\htdocs\power\templates\body.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6575308783186d5c2-20114156%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7cb8e69ccc99c728045bd6fa6ca4670443f1928a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\power\\templates\\body.tpl',
      1 => 1393066078,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6575308783186d5c2-20114156',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53087831afab19_94410486',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53087831afab19_94410486')) {function content_53087831afab19_94410486($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="wrapper">
	<h1 id="project-name"><?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['name'];?>
</h1>
	<div id="info">
		<div class="row">
			<div class="header"><img class="header-img" src="img/location.png"/></div>
			<div class="data"><?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['location'];?>
</div>
		</div>
		<div class="row">
			<div class="header"><img class="header-img" src="img/energy.png"/></div>
			<div class="data"><?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['capacity'];?>
MW</div>
		</div>
		<div class="row">
			<div class="header"><img class="header-img" src="img/fund.png"/></div>
			<div class="data">$<?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['fund_amount'];?>
M</div>
		</div>
		<div class="row">
			<div class="header"><img class="header-img" src="img/completion_time.png"/></div>
			<div class="data"><?php echo $_smarty_tpl->tpl_vars['data']->value['remain']->days;?>
 days</div>
		</div>
		<div class="start-time"><?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['start'];?>
</div>-<div class="end-time"><?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['end'];?>
</div>
	</div>
	<div><div class="demand"><div class="title">Power Demand</div><div class="data"><?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['de'];?>
 </div></div></div>
<div class="left">
	<h2>Current</h2>
	<div><div class="title">Shed</div><div class="data"><?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['su'];?>
</div></div>
	<div><div class="title">Current Loadshedding</div><div class="data"> <?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['ls_hr'];?>
</div></div>
</div>

<div class="right">
	<h2>After the completion of Projects</h2>
	<div><div class="title">New Shed</div><div class="data"> <?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['su_new'];?>
 </div></div>
	<div><div class="title">New Loadshedding</div><div class="data"> <?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['ls_hr_new'];?>
</div></div>
</div>
</div>
<?php }} ?>
