<?php /* Smarty version Smarty-3.1.16, created on 2014-02-22 11:04:49
         compiled from "/opt/lampp/htdocs/power/templates/body.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19562093395308754feb2bd1-40624793%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ed8884958859421b2e45a6b3bd41e73a1f297597' => 
    array (
      0 => '/opt/lampp/htdocs/power/templates/body.tpl',
      1 => 1393063488,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19562093395308754feb2bd1-40624793',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5308754fee0d63_84468755',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5308754fee0d63_84468755')) {function content_5308754fee0d63_84468755($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="wrapper">
	<h1 id="project-name"><?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['name'];?>
</h1>
	<div id="info">
		<div class="row">
			<div class="header"><img class="header-img" src="img/location.png"/></div>
			<div class="data"><?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['location'];?>
</div>
		</div>
		<div class="row">
			<div class="header"><img class="header-img" src="img/energy.png"/></div>
			<div class="data"><?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['capacity'];?>
MW</div>
		</div>
		<div class="row">
			<div class="header"><img class="header-img" src="img/fund.png"/></div>
			<div class="data">$<?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['fund_amount'];?>
M</div>
		</div>
		<div class="row">
			<div class="header"><img class="header-img" src="img/completion_time.png"/></div>
			<div class="data"><?php echo $_smarty_tpl->tpl_vars['data']->value['remain']->days;?>
 days</div>
		</div>
		<div class="start-time"><?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['start'];?>
</div>-<div class="end-time"><?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['end'];?>
</div>
	</div>
</div>

Name    <?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['name'];?>
 <br />
Location    <?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['location'];?>
 <br />
Type    <?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['type'];?>
 <br />
fund    <?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['fund'];?>
 <br />
fund_amount    $<?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['fund_amount'];?>
M <br />
capacity    <?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['capacity'];?>
 MW <br />
annual energy    <?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['annual_energy'];?>
 GWh <br />
start    <?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['start'];?>
 <br />
end    <?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['end'];?>
 <br />
status    <?php echo $_smarty_tpl->tpl_vars['data']->value['profile']['status'];?>
% <br />
remain <?php echo $_smarty_tpl->tpl_vars['data']->value['remain']->days;?>
days <br />
demand <?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['de'];?>
 <br />
supply <?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['su'];?>
 <br />
load <?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['ls_hr'];?>
 <br />
new supply <?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['su_new'];?>
 <br/>
load new <?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['ls_hr_new'];?>
 <br/>
<?php }} ?>
