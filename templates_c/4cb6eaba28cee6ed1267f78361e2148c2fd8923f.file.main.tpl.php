<?php /* Smarty version Smarty-3.1.16, created on 2014-02-22 11:55:35
         compiled from "C:\xampp\htdocs\power\templates\main.tpl" */ ?>
<?php /*%%SmartyHeaderCode:175925308782cdc90f7-34148612%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4cb6eaba28cee6ed1267f78361e2148c2fd8923f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\power\\templates\\main.tpl',
      1 => 1393066039,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '175925308782cdc90f7-34148612',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5308782cf21bb1_03148529',
  'variables' => 
  array (
    'data' => 0,
    'v' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5308782cf21bb1_03148529')) {function content_5308782cf21bb1_03148529($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="wrapper">
<h1>Onging Hydropower Projects</h1>
<ul id="projects">
<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['data']->value['const_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
  <li><a href="view.php?id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
</a></li>
<?php }
if (!$_smarty_tpl->tpl_vars['v']->_loop) {
?>
  Array is empty
<?php } ?>
</ul>

<div><div class="demand"><div class="title">Power Demand</div><div class="data"><?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['de'];?>
 </div></div></div>
<div class="left">
	<h2>Current</h2>
	<div><div class="title">Shed</div><div class="data"><?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['su'];?>
</div></div>
	<div><div class="title">Current Loadshedding</div><div class="data"> <?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['ls_hr'];?>
</div></div>
</div>

<div class="right">
	<h2>After the completion of Projects</h2>
	<div><div class="title">New Shed</div><div class="data"> <?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['su_new'];?>
 </div></div>
	<div><div class="title">New Loadshedding</div><div class="data"> <?php echo $_smarty_tpl->tpl_vars['data']->value['calc']['ls_hr_new'];?>
</div></div>
</div><?php }} ?>
