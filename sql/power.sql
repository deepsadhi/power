-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 22, 2014 at 11:06 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `power`
--

-- --------------------------------------------------------

--
-- Table structure for table `under_construction`
--

CREATE TABLE IF NOT EXISTS `under_construction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `fund` varchar(100) NOT NULL,
  `fund_amount` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `annual_energy` int(11) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `under_construction`
--

INSERT INTO `under_construction` (`id`, `name`, `location`, `type`, `fund`, `fund_amount`, `capacity`, `annual_energy`, `start`, `end`, `status`) VALUES
(1, 'Kulekhani III Hydroelectric Project ', 'Makwanpur', 'Cascade project of Kulekhani Storage', 'Government of Nepal and Nepal Electricity Authority (NEA)', 23, 14, 41, '2008-09-03', '2014-09-13', 65),
(4, 'Chameliya Hydroelectric Project ', 'Darchula', 'Run-off - river plant ', 'GoN, NEA and Korea', 0, 30, 184, '2007-01-02', '2014-03-15', 87),
(5, 'Upper Trishuli 3 A project', 'Rasuwa and Nuwakot', 'Run of River', 'China EXIM Bank', 120, 60, 490, '2011-06-01', '2020-08-10', 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
