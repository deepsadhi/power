<?php

define('DS', DIRECTORY_SEPARATOR);
define('Config\PREPARE_FORM', true);
require_once('..'.DS.'zLIB'.DS.'core'.DS.'init.inc.php');


$smarty = Smarty_Template::instance();
$smarty->assign('title', 'Sign Up');

$data = array();
$data['token'] = $_SESSION['token'];
$data['submitURL'] = $_SERVER['PHP_SELF'];

if(isset($_POST['signup']) && isset($_POST['token']) && $_POST['token'] == $_SESSION['token']){
    $purifier = new HTMLPurifier();

    $postData = array('name', 'email', 'password');

    $profile = array();
    $flag = true;
    
    foreach($postData as $pd){
        $var = $purifier->purify($_POST[$pd]);
        if(empty($var) || $var==''){
            $flag = false;
        } else {
            $profile[$pd] = $var;
        }
    }
    
    if(!$flag){
        $data['msg'] = 'Some field missing :(';
    }else{
        $user = new User;
        $res = $user->add($profile);
        if($res){
            $data['msg'] = 'User successfully created :)';
        }else{
            $data['msg'] = 'Some error occured :(';
        }
    }
}

$smarty->assign('data', $data);
$smarty->display('signup.tpl');
