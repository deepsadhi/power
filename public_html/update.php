<?php

if(isset($_GET['id'])){
    $id = preg_replace('/[^0-9]/', '', $_GET['id']);
    if(empty($id)){
        header("Location: ./");
        exit;
    }
}else{  
    header("Location: ./");
    exit;
}

define('DS', DIRECTORY_SEPARATOR);
define('Config\PREPARE_FORM', true);
define('Config\SESSION_CHECK', true);
require_once('..'.DS.'zLIB'.DS.'core'.DS.'init.inc.php');


$smarty = Smarty_Template::instance();
$smarty->assign('title', 'Update');

$data = array();
$data['token'] = $_SESSION['token'];
$data['submitURL'] = $_SERVER['PHP_SELF']."?id=".$id;

if(isset($_POST['signup']) && isset($_POST['token']) && $_POST['token'] == $_SESSION['token']){
    $purifier = new HTMLPurifier();

    $postData = array('name', 'email', 'password');

    $profile = array();
    $flag = true;
    
    foreach($postData as $pd){
        $var = $purifier->purify($_POST[$pd]);
        if(empty($var) || $var==''){
            $flag = false;
        } else {
            $profile[$pd] = $var;
        }
    }
    
    if(!$flag){
        $data['msg'] = 'Some field missing :(';
    }else{
        $user = new User;
        $res = $user->updateProfile($profile, 'id='.$id);
        if($res){
            header("Location: view.php?id={$id}");
        }else{
            $data['msg'] = 'Some error occured :(';
        }
    }
}

$session = $_SESSION['user'];
$data['user'] = array('name' => $session['name']);

$user = new User;
$user->find('id='.$id);
$res = $user->results;
$r = array_shift($res);

$data['id'] = $id;
$data['profile'] = $r;
$data['name'] = $r['name'];
$data['email'] = $r['email'];

$smarty->assign('data', $data);
$smarty->display('update.tpl');
