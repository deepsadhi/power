<?php

if(isset($_GET['id'])){
    $id = preg_replace('/[^0-9]/', '', $_GET['id']);
    if(empty($id)){
        header("Location: ./");
        exit;
    }
}else{  
    header("Location: ./");
    exit;
}


define('DS', DIRECTORY_SEPARATOR);
require_once('..'.DS.'zLIB'.DS.'core'.DS.'init.inc.php');



$smarty = Smarty_Template::instance();
$smarty->assign('title', 'Profile');


$data = array();

$db = new Database;
$db->findById('under_construction', $id);
$res = $db->results;
$r = array_shift($res);


$data['profile'] = $r;


$date1=date_create($r['start']);
$date2=date_create($r['end']);
$data['remain']=date_diff($date1,$date2);


$load = array();
$load['de'] = 1094.62;
$load['su'] = 375;
$load['ls_hr'] = 12;

$load['su_new'] = $load['su']-floatval($r['capacity']);
$load['ls_hr_new'] = 12/$load['su']*$load['su_new'];

$data['calc'] = $load;

$smarty->assign('data', $data);
$smarty->display('body.tpl');
