<?php

define('DS', DIRECTORY_SEPARATOR);
define('Config\LOAD_LOG4PHP', false);
require_once('..'.DS.'zLIB'.DS.'core'.DS.'init.inc.php');


// First create a smarty object
$smarty = Smarty_Template::instance();


// To assign a single variable 
$var = 'This is a variable';
$smarty->assign('var', $var);


// To assign an array
$array = array();
$array['key'] = 'This is a array value';
$smarty->assign('array',        $array);


// For dropdown menu
$team = array(  '68010' => 'Ashmita',
                '68012' => 'Ayush',
                '68018' => 'Deepak',
                '68032' => 'Kshitiz',
                '68035' => 'Manisa'
             );
$smarty->assign('myOptions',    $team);
$smarty->assign('mySelect',     68012);


// Display the template
$smarty->display('sample.tpl');

