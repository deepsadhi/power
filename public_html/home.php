<?php

define('DS', DIRECTORY_SEPARATOR);
define('Config\PREPARE_FORM', true);
require_once('..'.DS.'zLIB'.DS.'core'.DS.'init.inc.php');


$smarty = Smarty_Template::instance();
$smarty->assign('title', 'Sign In');

$data = array();
$data['token'] = $_SESSION['token'];
$data['submitURL'] = $_SERVER['PHP_SELF'];

if(isset($_POST['signin']) && isset($_POST['token']) && $_POST['token'] == $_SESSION['token']){
    $purifier = new HTMLPurifier();

    $postData = array('email', 'password');

    $profile = array();
    $flag = true;
    
    foreach($postData as $pd){
        $var = $purifier->purify($_POST[$pd]);
        if(empty($var) || $var==''){
            $flag = false;
        } else {
            $profile[$pd] = $var;
        }
    }
    
    if(!$flag){
        $data['msg'] = 'Some field missing :(';
    }else{
        $user = new User;
        $res = $user->authenticate($profile);
        if(true === $res){
            header('Location: main.php');
            exit;
        }else{
            $data['msg'] = 'Invalid! username/password';
        }
    }
}

$smarty->assign('data', $data);
$smarty->display('home.tpl');
